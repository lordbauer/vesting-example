source getTxFunc.sh 

getInputTx $1

WALLETUTXO=${SELECTED_UTXO}

cardano-cli transaction build \
    --babbage-era \
    $TESTNET \
    --change-address $(cat $WALLET/$1.addr) \
    --tx-in $WALLETUTXO \
    --tx-out $(cat vestingParameterized.addr)+20000000 \
    --tx-out-datum-hash $(cat myDatumHash) \
    --out-file tx.body

cardano-cli transaction sign \
    --tx-body-file tx.body \
    --signing-key-file $WALLET/$1.skey \
    $TESTNET \
    --out-file tx.signed

cardano-cli transaction submit \
    $TESTNET \
    --tx-file tx.signed
