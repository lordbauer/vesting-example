source getTxFunc.sh 

getInputTx $1
WALLETUTXO=${SELECTED_UTXO}

getContractInputTx vestingParameterized
CONTRACTUTXO=${SELECTED_UTXO_CONTRACT}


# prewallet1: 1b8680686ecd06633fbe9e464a193da01485fe3f11193f142ca5a2e1
# prewallet:  cc9160d09d1afc6a72c0e580e0a34356b3714efee3d1eed17983896c

cardano-cli transaction build \
    --babbage-era \
    $TESTNET \
    --change-address $(cat $WALLET/$1.addr) \
    --tx-in $WALLETUTXO \
    --tx-in $CONTRACTUTXO \
    --tx-in-script-file vestingParameterized.plutus \
    --tx-in-datum-file myDatum.json \
    --tx-in-redeemer-file myRedemeerDatumAmount.json \
    --tx-in-collateral $WALLETUTXO \
    --invalid-before 4293117 \
    --tx-out $(cat $WALLET/$2.addr)+5000000 \
    --protocol-params-file protocol.json \
    --out-file tx.body

cardano-cli transaction sign \
    --tx-body-file tx.body \
    --signing-key-file $WALLET/$1.skey \
    $TESTNET \
    --out-file tx.signed

cardano-cli transaction submit \
    $TESTNET \
    --tx-file tx.signed
