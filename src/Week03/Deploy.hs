{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}

module Week03.Deploy
    ( writeJSON
    , writeValidator
    , writeMyRedeemer
    , writeVestingMyDatum
    , writeVestingParameterizedValidator
    , writeVestingValidator
    , writeVestingDatum
    , writeUnitRedeemer
    ) where

import           Cardano.Api
import           Cardano.Api.Shelley   (PlutusScript (..))
import           Codec.Serialise       (serialise)
import           Data.Aeson            (encode)
import qualified Data.ByteString.Lazy  as LBS
import qualified Data.ByteString.Short as SBS
import           PlutusTx              (Data (..))
import qualified PlutusTx
import qualified Ledger

import           Week03.Parameterized  as Parameterized 
import           Week03.Vesting        as Vesting

dataToScriptData :: Data -> ScriptData
dataToScriptData (Constr n xs) = ScriptDataConstructor n $ dataToScriptData <$> xs
dataToScriptData (Map xs)      = ScriptDataMap [(dataToScriptData x, dataToScriptData y) | (x, y) <- xs]
dataToScriptData (List xs)     = ScriptDataList $ dataToScriptData <$> xs
dataToScriptData (I n)         = ScriptDataNumber n
dataToScriptData (B bs)        = ScriptDataBytes bs

writeJSON :: PlutusTx.ToData a => FilePath -> a -> IO ()
writeJSON file = LBS.writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . dataToScriptData . PlutusTx.toData

writeValidator :: FilePath -> Ledger.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV1) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Ledger.unValidatorScript

-- Parameterized
writeMyRedeemer :: IO ()
writeMyRedeemer = writeJSON "testnet/myRedemeerDatumAmount.json" DatumAmount --OwnAmount

writeVestingMyDatum :: IO () 
writeVestingMyDatum = writeJSON "testnet/myDatum.json" (MyDatum 5000000)

writeVestingParameterizedValidator :: IO (Either (FileError ()) ())
writeVestingParameterizedValidator = writeValidator "testnet/vestingParameterized.plutus" $ Parameterized.validator $ VestingParam
    { Parameterized.beneficiary = Ledger.PaymentPubKeyHash "2a0b4833af1a0f1bb7040b289c13147734e531d66cab868330c55f92"
    , Parameterized.deadline    = 1643235300001
    }

-- Vesting
writeVestingDatum :: IO () 
writeVestingDatum = writeJSON "testnet/vestingDatum.json" (Vesting.VestingDatum (Ledger.PaymentPubKeyHash "2a0b4833af1a0f1bb7040b289c13147734e531d66cab868330c55f92") 1643235300001)

writeUnitRedeemer :: IO () 
writeUnitRedeemer = writeJSON "testnet/unitRedeemer.json" ()

writeVestingValidator :: IO (Either (FileError ()) ())
writeVestingValidator = writeValidator "testnet/vesting.plutus" Vesting.validator
