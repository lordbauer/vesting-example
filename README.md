
# Vesting example

Build upon [plutus-pioneer-program-week03](https://github.com/input-output-hk/plutus-pioneer-program/tree/main/code/week03). Two contracts to lock funds for a beneficiary with a deadline. The first one Parameterized.hs defines them in the instances, each change gives a new address. The second one defines in the datum, the user who locks funds defines beneficiary and deadline.

## Prerequisite

* [The Plutus Application Framework](https://github.com/input-output-hk/plutus-apps)
* [cardano-node](https://github.com/input-output-hk/cardano-node)
* 2 Pre-Production testnet [wallets](https://developers.cardano.org/docs/stake-pool-course/handbook/keys-addresses/)

## build project, validator,  necessary data to build transaction

* change values to your own in Deploy.hs
* run `cabal update` from root
*  `cabal build`
*  `cabal repl`
* write your validator/datum/redeemer defined in Deploy.hs

## use on testnet (cardano-cli)
* start your node (Pre-Production testnet)

*  `cd testnet`

* change variables in env.sh and run `. env.sh`

* build contract address `cardano-cli address build --payment-script-file vestingParameterized.plutus $TESTNET --out-file vestingParameterized.addr`

*  `./balance.sh walletName` shows utxos at given wallet name

*  `./contractBalance vestingParameterized` shows utxos at parameterized vesting contract

*  `./giveParameterized.sh walletName` locks funds at vestingParameterized.addr

*  `./grabOwnAmount.sh walletName` unlocks utxo, only if correct signer and deadline reached

*  `./grabDatumAmount walletName walletNameBeneficiary` unlocks utxo, only if the amount defined in the datum is sent to the beneficiary and the deadline reached